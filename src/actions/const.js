export const TEST = 'TEST';

export const SCHEMA_FETCHING = 'SCHEMA_FETCHING';
export const SCHEMA_FETCHED = 'SCHEMA_FETCHED';
export const SCHEMA_ERROR = 'SCHEMA_ERROR';

export const DATA_FETCHING = 'DATA_FETCHING';
export const DATA_FETCHED = 'DATA_FETCHED';
export const DATA_ERROR = 'DATA_ERROR';
