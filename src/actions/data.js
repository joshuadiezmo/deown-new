import {DATA_FETCHING, DATA_FETCHED, DATA_ERROR} from './const';
const Parse = require('parse');
const ParseError = require('../helpers/ParseError');

function action(className, page = 1) {
	let perPage = 20;
	return async(dispatch) => {
		try {
			if (!className || className == '')
				throw Error('Class name is required.');

			let query = new Parse.Query(className);
			query.skip((page - 1) * perPage);
			query.limit(perPage);
			dispatch({type: DATA_FETCHING});
			let objects = await query.find({useMasterKey: true});
			dispatch({type: DATA_FETCHED, data: objects, className, page});
		} catch (err) {
			dispatch({type: DATA_ERROR, error: {message: err.message || JSON.stringify(err)}});
		}
	}
}

module.exports = action;
