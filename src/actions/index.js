/* Exports all the actions from a single point.

 Allows to import actions like so:

 import {action1, action2} from '../actions/'
 */
/* Populated by react-webpack-redux:action */
import test from '../actions/test.js';
import schemas from '../actions/schemas.js';
import data from '../actions/data.js';

const actions = {
	test,
	schemas,
	data
};
module.exports = actions;
