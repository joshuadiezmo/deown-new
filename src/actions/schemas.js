import {SCHEMA_FETCHING, SCHEMA_FETCHED, SCHEMA_ERROR} from './const';
const request = require('../helpers/request');
const config = require('../config.json');
const ParseError = require('../helpers/ParseError');

function action() {
	return async(dispatch) => {
		dispatch({type: SCHEMA_FETCHING});
		try {
			let response = await request.get('/schemas');
			if (response.status)
				dispatch({type: SCHEMA_FETCHED, data: response.data.results, config});
			else
				throw Error(response.data);
		} catch (err) {
			ParseError(err);
		}
	};
}

module.exports = action;
