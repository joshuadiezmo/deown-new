import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import DisplayFile from './DisplayFile';
import DisplayPointer from './DisplayPointer';
import DisplayGeoPoint from './DisplayGeoPoint';

class Display extends Component {
	render() {
		let {column, object} = this.props;
		let data = object.get(column.name);
		let element = null;
		if(!data)
			element = null;
		else if (column.name == 'objectId')
			element = object.id;
		else if (column.type == 'Date' || column.type == 'Number' || column.type == 'Boolean')
			element = data ? data.toLocaleString() : '';
		else if (column.type == 'String')
			element = data;
		else if (column.type == 'File')
			element = <DisplayFile data={object.get(column.name)}/>;
		else if (column.type == 'Pointer')
			element = <DisplayPointer data={object.get(column.name)}/>;
		else if (column.type == 'GeoPoint')
			element = <DisplayGeoPoint data={object.get(column.name)}/>;
		else
			element = JSON.stringify(data);
		return element;
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Display);
