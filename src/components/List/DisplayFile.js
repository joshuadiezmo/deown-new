import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';

class DisplayFile extends Component {
	render() {
		let data = this.props.data;
		if (data) {
			let name = data.name();
			let url = data.url();
			return (
				<div>
					<a href={url} target="_blank">
						{name.length > 30 ? '...'+name.substring(name.length - 30, name.length) : name}
					</a>
				</div>
			);
		} else {
			return (
				<div></div>
			)
		}
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayFile);
