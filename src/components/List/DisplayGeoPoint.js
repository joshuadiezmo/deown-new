import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';

class DisplayGeoPoint extends Component {
	render() {
		let {data} = this.props;
		if (data) {
			let lat = data.latitude;
			let lng = data.longitude;
			return (
				<div>
					<a href={`http://www.google.com/maps/place/${lat},${lng}`} target="_blank">{`${lat},${lng}`}</a>
				</div>
			);
		} else
			return null;
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayGeoPoint);
