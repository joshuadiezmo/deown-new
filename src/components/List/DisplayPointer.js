import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';

class DisplayPointer extends Component {
	render() {
		let {data} = this.props;
		if (data) {
			return (
				<div>
					<a href={`#${this.props.data.id}`}>{this.props.data.id}</a>
				</div>
			);
		} else
			return null;
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(DisplayPointer);
