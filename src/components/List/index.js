import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {schemas, data} from 'actions';
import {Table, Container, Row, Col, Button} from 'reactstrap';
import Display from './Display';

const Parse = require('parse');

class List extends Component {
	state = {
		objects: []
	};

	componentWillReceiveProps(nextProps) {
		this.fetchData(nextProps);
	}

	fetchData = (props, force) => {
		let className = this.props.match.params.className;
		let newClassname = props.match.params.className;
		let schemas = this.props.schemas;
		if (className != newClassname || force) {
			this.props.actions.data(newClassname, 1);
			// let query = new Parse.Query(newClassname);
			// query.find({useMasterKey: true})
			// 	.then(objects => this.setState({objects}))
			// 	.catch(err => alert(err.message));
		}
	};

	componentDidMount() {
		this.fetchData(this.props, true);
		this.props.actions.schemas();
	}

	classData = () => {
		let {schemas} = this.props;
		let className = this.props.match.params.className;
		let data = schemas.data;
		let classData = {};
		data.map(item => {
			if (item.className == className)
				classData = item;
		});
		return classData;
	};

	columns = () => {
		let classData = this.classData();
		let fields = classData.fields || {};
		return fields;
	};

	columnsDisplay = () => {
		let columns = this.columns();
		let excepts = ['ACL', 'objectId'];
		let columnsDisplay = [];
		let count = 0;
		for (let key in columns) {
			if (count <= 5 && excepts.indexOf(key) < 0) {
				columnsDisplay.push({name: key, ...columns[key]});
				count++;
			}
		}
		return columnsDisplay;
	};

	render() {
		let className = this.props.match.params.className;
		let {data, loading, error} = this.props.data;
		let objectsList = data[className] || {};
		let objects = objectsList[1] || [];
		return (
			<div>
				<Container>
					<Row>
						<Col>
							<div style={{float: 'left'}}>
								<h3>
									{className.prettify()}
									{loading ? <i className="fa fa-spinner fa-pulse fa-fw" style={{
											verticalAlign: 'middle',
											fontSize: 14,
											marginLeft: 5
										}}/> : null}
								</h3>
							</div>
							<div style={{float: 'right'}}>
								<Button size="sm" color="primary">Create</Button>
							</div>
						</Col>
					</Row>
					<Row>
						<Col>
							<Table bordered responsive>
								<thead>
								<tr>
									{
										this.columnsDisplay().map(column => {
											return (<th key={`list-th-${column.name}`}>{column.name.prettify()}</th>)
										})
									}
								</tr>
								</thead>
								<tbody>
								{
									objects.map(object => {
										return (
											<tr key={`list-tr-${object.id}`}>{
												this.columnsDisplay().map(column => {
													return (
														<td key={`list-tr-${object.id}-${column.name}`}>
															<Display object={object} column={column}/>
														</td>
													);
												})
											}</tr>
										)
									})
								}
								</tbody>
							</Table>
						</Col>
					</Row>
				</Container>
				{/*{JSON.stringify(objects)}*/}
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	const {schemas, data} = state;
	const props = {schemas, data};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {schemas, data};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(List);
