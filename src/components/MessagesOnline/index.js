import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {ListGroup, ListGroupItem} from 'reactstrap';

const moment = require('moment');

class MessagesOnline extends Component {
	render() {
		return (
			<div className="messages-online">
				<ListGroup>
					<ListGroupItem tag="a" href="#" action className="messages-online-item-list">
						<div className="image-container">
							<img src="http://via.placeholder.com/300x300" alt="user" className="image"/>
						</div>
						<div className="text-container">
							<div className="name-time">
								<span className="name">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aut blanditiis consequatur culpa cupiditate deleniti eius, eos eum incidunt labore minus nam odit possimus repellat repellendus sapiente sint tempora, temporibus velit!</span>
								<div>
									<span>{moment().format('hh:mm a')}</span>
								</div>
							</div>
						</div>
					</ListGroupItem>
					<ListGroupItem tag="a" href="#" action>Dapibus ac facilisis in</ListGroupItem>
					<ListGroupItem tag="a" href="#" action>Morbi leo risus</ListGroupItem>
					<ListGroupItem tag="a" href="#" action>Porta ac consectetur ac</ListGroupItem>
					<ListGroupItem tag="a" href="#" action>Cras justo odio</ListGroupItem>
				</ListGroup>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(MessagesOnline);
