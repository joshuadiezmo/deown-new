import React, {Component} from 'react';
import {
	BrowserRouter as Router,
	Route as RouteWithoutLayout,
	Switch
} from 'react-router-dom';

import {applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {Provider} from 'react-redux';
import configureStore from 'store';

import Sidebar from 'react-sidebar';
import SideMenu from './sideMenu';

import Route from './route';
import Home from './pages/home';
import Items from './pages/items';
import Item from './pages/item';
import Post from './pages/post';
import Profile from './pages/profile';
import Login from './login';
import List from './List';

const store = configureStore(applyMiddleware(thunk));

export default class App extends Component {
	state = {
		sidebarOpen: true
	};

	onSetSidebarOpen = (open) => {
		this.setState({sidebarOpen: open});
	};

	render() {
		return (
			<Provider store={store} id="app">
				<Router>
					<Switch>
						<Route exact path="/" component={Home} fullWidth/>
						<Route path="/items" component={Items} fullWidth/>
						<Route exact path="/item/:item_id" component={Item} fullWidth/>
						<Route path="/post" component={Post} fullWidth/>
						<Route exact path="/profile" component={Profile}/>
						<Route path="/user/:user" component={Profile} fullWidth/>
						<Route path="/class/:className" component={List}/>
						<RouteWithoutLayout exact path="/login" component={Login}/>
					</Switch>
				</Router>
			</Provider>
		);
	}
}
