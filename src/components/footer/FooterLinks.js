import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import LinkSitemap from './LinkSitemap';

const moment = require('moment');

class FooterLinks extends Component {
	render() {
		return (
			<div className="footer-links">
				<Container>
					<Row>
						<Col md={4} xs={12} className="footer-link-item-container">
							<LinkSitemap/>
						</Col>
						<Col md={4} xs={12} className="footer-link-item-container">
							<h5 className="title">HEADQUARTERS</h5>
							<p style={{color: '#888', fontSize: '10pt'}}>
								Rm 1630, Park-in Commercial Centre, 56 Dundas St,
								<br/>
								Mong Kok, Hong Kong
								<br/>
								<br/>
								Monday - Friday 11:00am - 8:00pm
								<br/>
								Saturday and Sunday: By Reservation
							</p>
						</Col>
						<Col md={4} xs={12} className="footer-link-item-container">
							<h5 className="title">SUPPORT</h5>
							<div className="footer-support-item">
								<div className="icon">
									<i className="fa fa-phone"/>
								</div>
								<div className="details">
									<b>+852 6036 5759</b>
									<span>Monday - Friday 11:00am - 8:00pm</span>
								</div>
							</div>
							<div className="footer-support-item">
								<div className="icon">
									<i className="fa fa-comments"/>
								</div>
								<div className="details">
									<b>
										SUPPORT
									</b>
									<span>
										Click here to start chatting
									</span>
								</div>
							</div>
							<div className="footer-support-item">
								<div className="icon">
									<i className="fa fa-envelope"/>
								</div>
								<div className="details">
									<b>
										EMAIL ADDRESS
									</b>
									<span>
										rent@de-own.com
									</span>
								</div>
							</div>
						</Col>
					</Row>
					<Row style={{marginTop: 40,marginBottom:40}}>
						<Col md={3} xs={12} style={{textAlign:'center'}}>
							<small>
								Copyright {moment().year()}. De-own
							</small>
						</Col>
						<Col style={{fontSize:'9pt',textAlign:'center'}} md={4} xs={12}>
							<i className="fa fa-globe" style={{marginRight:10}}/>
							<select name="language" id="language" style={{width:150}}>
								<option value="english">English</option>
							</select>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(FooterLinks);
