import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col, Nav, NavItem, NavLink, Navbar,NavbarBrand} from 'reactstrap';
import {LinkContainer} from 'react-router-bootstrap';

const logo = require('../../images/logo.png');

class FooterNav extends Component {
	render() {
		return (
			<div className="footer-nav d-none d-lg-block">
				<Container>
					<Row>
						<Col>
							<Navbar expand="md" light>
								<LinkContainer to="/">
									<NavbarBrand>
										<img src={logo} style={{width: 120}}/>
									</NavbarBrand>
								</LinkContainer>
								<Nav navbar className="mr-auto">
									<NavItem>
										<NavLink href="#">
											Camera
										</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="#">
											Camera Accessories
										</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="#">
											Drone
										</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="#">
											Drone Accessories
										</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="#">
											Game Console
										</NavLink>
									</NavItem>
									<NavItem>
										<NavLink href="#">
											Others
										</NavLink>
									</NavItem>
								</Nav>
							</Navbar>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(FooterNav);
