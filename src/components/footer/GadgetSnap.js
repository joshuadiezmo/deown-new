import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';

const spriteController = require('../../images/sprites.png');
class GadgetSnap extends Component {
	render() {
		return (
			<Container>
				<Row>
					<Col>
						<div className="gadget-snap-container">
							<div className="gadget-sprite">
								<img src={spriteController}/>
							</div>
							<div className="gadget-text">
								<div>
									<h4 style={{margin: 0}}>Got your gadget rented in a Snap!</h4>
									<span>
											Snap a photo for others to Rent. They can reach you with just a Chat.{' '}
										<a href="#">LearnMore</a>
										</span>
								</div>
							</div>
						</div>
					</Col>
				</Row>
			</Container>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(GadgetSnap);
