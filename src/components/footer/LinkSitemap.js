import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';

class LinkSitemap extends Component {
	render() {
		return (
			<div className="site-map">
				<h5 className="title">SITEMAP</h5>
				<Row>
					<Col xs={6}>
						<a href="#">
							Home
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Checkout
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Rent goods
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							FAQ
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Contact us
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Terms of use
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							My account
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Privacy Policy
						</a>
					</Col>
					<Col xs={6}>
						<a href="#">
							Shopping cart
						</a>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(LinkSitemap);
