import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import GadgetSnap from './GadgetSnap';
import FooterNav from './FooterNav';
import FooterLinks from './FooterLinks';

class Footer extends Component {
	render() {
		return (
			<div>
				<div className="footer">
					<GadgetSnap/>
					<FooterNav/>
					<FooterLinks/>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Footer);
