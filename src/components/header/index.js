import React, {Component} from 'react';
import {
	Collapse,
	Navbar,
	NavbarToggler,
	NavbarBrand,
	Nav,
	NavItem,
	NavLink,
	UncontrolledDropdown,
	DropdownToggle,
	DropdownMenu,
	DropdownItem,
	Container,
	Button
} from 'reactstrap';
import {LinkContainer} from 'react-router-bootstrap';
const Parse = require('parse');
const logo = require('../../images/logo.png');

export default class Header extends Component {

	constructor(props) {
		super(props);
		this.state = {
			isOpen: false
		};
	}

	toggle = () => {
		this.setState({
			isOpen: !this.state.isOpen
		});
	};

	renderCategories = () => {
		let cats = ['camera', 'camera_accessories', 'drone', 'drone_accessories', 'game_console', 'others'];
		let location = this.props.location || {};
		return cats.map(cat => {
			return (
				<LinkContainer to={`/items?cat=${cat}`} key={cat}
							   active={location.pathname + location.search == `/items?cat=${cat}`}>
					<DropdownItem>
						{cat.prettify()}
					</DropdownItem>
				</LinkContainer>
			)
		})
	};

	render() {
		return (
			<Navbar color="white" light expand="md" fixed="top">
				<Container>
					<LinkContainer to="/">
						<NavbarBrand>
							<img src={logo} style={{width: 120}}/>
						</NavbarBrand>
					</LinkContainer>
					<NavbarToggler onClick={this.toggle}/>
					<Collapse isOpen={this.state.isOpen} navbar>
						<Nav className="mr-auto" navbar>
							<NavItem>
								<UncontrolledDropdown>
									<DropdownToggle nav caret>
										Categories
									</DropdownToggle>
									<DropdownMenu >
										{this.renderCategories()}
									</DropdownMenu>
								</UncontrolledDropdown>
							</NavItem>
							<NavItem>
								<NavLink href="/components/">How It Works</NavLink>
							</NavItem>
						</Nav>

						<Nav className="ml-auto" navbar>
							<UncontrolledDropdown nav>
								<DropdownToggle nav caret>
									Username
								</DropdownToggle>
								<DropdownMenu >
									<DropdownItem>
										Profile
									</DropdownItem>
									{/*<DropdownItem divider/>*/}
									<DropdownItem>
										Logout
									</DropdownItem>
								</DropdownMenu>
							</UncontrolledDropdown>
							<NavItem>
								<NavLink href="/components/">
									<i className="fa fa-envelope-o"/>
								</NavLink>
							</NavItem>
							<NavItem>
								<NavLink href="https://github.com/reactstrap/reactstrap">
									<i className="fa fa-shopping-basket"/>
								</NavLink>
							</NavItem>
							<NavItem style={{display: 'flex'}}>
								<LinkContainer to="/post" style={{margin: 'auto 0'}}>
									<Button size="sm" color="primary">
										POST
									</Button>
								</LinkContainer>
							</NavItem>
						</Nav>
					</Collapse>
				</Container>
			</Navbar>
		);
	}
}
