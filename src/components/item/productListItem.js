import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Link} from 'react-router-dom';

class ProductListItem extends Component {
	makeid = () => {
		var text = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

		for (var i = 0; i < 6; i++)
			text += possible.charAt(Math.floor(Math.random() * possible.length));

		return text;
	};

	render() {
		let id = this.makeid();
		return (
			<div className="product-list-item">
				<Link to={`/item/${id}`}>
					<div className="image" style={{backgroundImage: `url(http://via.placeholder.com/200x350)`}}/>
				</Link>
				<div className="product-info">
					<div>
						<Link to={`/item/${id}`} style={{color:'#333'}}>
							<p style={{margin: 0}}><b>HKD 10</b> test</p>
						</Link>
						<div>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
						</div>
						<small>0 review</small>
					</div>
					<div className="heart">
						<span className="count">
							0
						</span>
						<i className="fa fa-heart-o fa-3x"/>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ProductListItem);
