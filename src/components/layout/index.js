import React, {Component} from 'react';

import Header from '../header';
import Footer from '../footer';
import {Container, Row, Col} from 'reactstrap';

export default class Layout extends Component {

	render() {
		let fullWidth = this.props.fullWidth || false;
		return (
			<div id="app" style={{position:'relative',paddingTop: 49, paddingBottom: 30}}>
				<Header {...this.props}/>
				{
					fullWidth === true ? this.props.children : (
							<Container>
								<Row>
									<Col md={12}>
										{this.props.children}
									</Col>
								</Row>
							</Container>
						)
				}
				<Footer/>
			</div>
		);
	}
}
