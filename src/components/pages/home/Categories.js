import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';
import {Link} from 'react-router-dom';

const category1 = require('../../../images/category-1.jpg');
const category2 = require('../../../images/category-2.jpg');
const category3 = require('../../../images/category-3.jpg');
const category4 = require('../../../images/category-4.jpg');

class Categories extends Component {
	render() {
		return (
			<div style={{marginTop: 30}} className="d-none d-md-block">
				<Row>
					<Col>
						<h3 style={{fontWeight: 'bolder'}}>
							CATEGORIES
						</h3>
					</Col>
				</Row>
				<Row className="row-eq-height" noGutters>
					<Col xs={4} style={{paddingRight: 10}}>
						<div className="category-item" style={{backgroundImage: `url(${category1})`}}>
							<div className="category-label-container">
								<Link to="/items?cat=camera">
									<div className="category-label">
										<h5>CAMERAS</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
								<Link to="/items?cat=camera_accessories">
									<div className="category-label">
										<h5>ACCESSORIES</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
							</div>
						</div>
					</Col>
					<Col xs={4} style={{paddingRight: 10}}>
						<div className="category-item" style={{backgroundImage: `url(${category2})`}}>
							<div className="category-label-container">
								<Link to="/items?cat=drone">
									<div className="category-label">
										<h5>DRONES</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
								<Link to="/items?cat=drone_accessories">
									<div className="category-label">
										<h5>ACCESSORIES</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
							</div>
						</div>
					</Col>
					<Col xs={4}>
						<div className="category-item" style={{
							backgroundImage: `url(${category3})`,
							height: 125,
							marginBottom: 5,
							marginLeft: 0
						}}>
							<div className="category-label-container">
								<Link to="/items?cat=game_console">
									<div className="category-label" style={{width: 230}}>
										<h5>GAME CONSOLES</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
							</div>
						</div>
						<div className="category-item"
							 style={{backgroundImage: `url(${category4})`, height: 125, marginTop: 5, marginLeft: 0}}>
							<div className="category-label-container">
								<Link to="/items?cat=others">
									<div className="category-label" style={{width: 230}}>
										<h5>OTHERS</h5>
										<span>
									0 items available
								</span>
									</div>
								</Link>
							</div>
						</div>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Categories);
