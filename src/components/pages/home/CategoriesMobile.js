import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';
import {LinkContainer} from 'react-router-bootstrap';

const category1 = require('../../../images/cat-mobile-1.png');
const category2 = require('../../../images/cat-mobile-2.png');
const category3 = require('../../../images/cat-mobile-3.png');
const category4 = require('../../../images/cat-mobile-4.png');
const category5 = require('../../../images/cat-mobile-5.png');
const category6 = require('../../../images/cat-mobile-6.png');

class CategoriesMobile extends Component {
	render() {
		return (
			<div style={{marginTop: 30}} className="d-block d-md-none">
				<Row>
					<Col>
						<h3 style={{fontWeight: 'bolder'}}>
							CATEGORIES
						</h3>
					</Col>
				</Row>
				<Row>
					<LinkContainer to="/items?cat=camera">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category1})`}}>
								<div>
									<i className="fa fa-camera-retro"/>
									<span className="title">Cameras</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
					<LinkContainer to="/items?cat=camera_accessories">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category2})`}}>
								<div>
									<i className="fa fa-camera-retro"/>
									<span className="title">Camera Accessories</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
					<LinkContainer to="/items?cat=drone">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category3})`}}>
								<div>
									<i className="fa fa-paper-plane"/>
									<span className="title">Drones</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
					<LinkContainer to="/items?cat=drone_accessories">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category4})`}}>
								<div>
									<i className="fa fa-paper-plane"/>
									<span className="title">Drone Accessories</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
					<LinkContainer to="/items?cat=game_console">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category5})`}}>
								<div>
									<i className="fa fa-gamepad"/>
									<span className="title">Game Consoles</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
					<LinkContainer to="/items?cat=others">
						<Col className="category-mobile" xs={12}>
							<div className="category-mobile-item" style={{backgroundImage: `url(${category6})`}}>
								<div>
									<i className="fa fa-cog"/>
									<span className="title">Others</span>
								</div>
								<span className="count">
								3
							</span>
							</div>
						</Col>
					</LinkContainer>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(CategoriesMobile);
