import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';
import ProductListItem from '../../item/productListItem';

class FeaturedList extends Component {
	renderItems = () => {
		let items = Array.apply(null, Array(5));
		return items.map((item,index)=>{
			return(
				<Col xs={6} key={index} className="list-item-md">
					<ProductListItem/>
				</Col>
			);
		});
	};

	render() {
		return (
			<div style={{marginTop: 30}}>
				<Row>
					<Col>
						<h3 style={{fontWeight: 'bolder'}}>
							FEATURED LISTINGS
						</h3>
					</Col>
				</Row>
				<Row>
					{this.renderItems()}
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedList);
