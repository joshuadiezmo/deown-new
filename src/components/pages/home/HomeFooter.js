import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import OwlCarousel from 'react-owl-carousel';

class HomeFooter extends Component {
	items = [
		{
			icon: 'shopping-cart',
			title: 'RESERVED',
			text: 'Browse items within the page,select the items you want to rent to the shopping cart.'
		},
		{icon: 'credit-card', title: 'PAYMENT', text: 'To facilitate your way to pay'},
		{
			icon: 'check-square',
			title: 'CONFIRM',
			text: 'The clerk will contact you within 1 business day to confirm the rental details.'
		},
		{icon: 'truck', title: 'PICK UP', text: 'According to the way you choose to pick up the goods you rent.'},
	];

	state = {
		display: false
	};

	componentDidMount() {
		setTimeout(() => {
			this.setState({display: true})
		}, 100);
	}

	render() {
		let {display} = this.state;
		if (display)
			return (
				<div style={{marginTop: 70}} className="home-footer">
					<Container>
						<Row>
							<Col>
								<OwlCarousel className="owl-theme" margin={10} nav items={4}
											 responsive={{
												 0: {items: 1},
												 768: {items: 2},
												 992: {items: 4}
											 }}>
									{this.items.map((item, index) => {
										return (
											<div className="item" key={`home-footer-item-${index}`}>
												<div>
													<h3>
														<i className={`fa fa-${item.icon}`}/> {item.title}
													</h3>
													<p>
														{item.text}
													</p>
												</div>
												{index < (this.items.length - 1) ? (
														<div className="arrow-next">
															<i className="fa fa-chevron-right"/>
														</div>
													) : null}
											</div>
										)
									})}
								</OwlCarousel>
							</Col>
						</Row>
					</Container>
				</div>
			);
		else
			return null;
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeFooter);
