import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {CarouselItem, CarouselCaption, Carousel, CarouselIndicators, CarouselControl} from 'reactstrap';

const ImagePlaceholder = require('../../../images/carousel-img/phantom-featured.png');

class ItemsCarousel extends Component {
	state = {
		items: [
			{
				src: ImagePlaceholder,
				altText: 'Slide 1',
				caption: 'Slide 1'
			},
			{
				src: ImagePlaceholder,
				altText: 'Slide 2',
				caption: 'Slide 2'
			},
			{
				src: ImagePlaceholder,
				altText: 'Slide 3',
				caption: 'Slide 3'
			}
		],
		activeIndex: 0
	};
	onExiting = () => {
		this.animating = true;
	};

	onExited = () => {
		this.animating = false;
	};

	next = () => {
		let {items} = this.state;
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
		this.setState({activeIndex: nextIndex});
	};

	previous = () => {
		let {items} = this.state;
		if (this.animating) return;
		const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
		this.setState({activeIndex: nextIndex});
	};

	goToIndex = (newIndex) => {
		if (this.animating) return;
		this.setState({activeIndex: newIndex});
	};

	render() {
		let {items, activeIndex} = this.state;
		const slides = items.map((item,index) => {
			return (
				<CarouselItem
					onExiting={this.onExiting}
					onExited={this.onExited}
					key={`home-carousel-item-${index}`}
					src={item.src}
					className="item-image-carousel"
				>
					{/*<CarouselCaption captionText={item.caption} captionHeader={item.caption}/>*/}
				</CarouselItem>
			);
		});
		return (
			<Carousel
				activeIndex={activeIndex}
				next={this.next}
				previous={this.previous}
				className="home-carousel"
			>
				<CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex}/>
				{slides}
			</Carousel>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemsCarousel);
