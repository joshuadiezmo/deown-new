import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col, Form, Input, InputGroup, InputGroupAddon, Button} from 'reactstrap';

const searchBG = require('../../../images/search-bg.png');

class SearchForm extends Component {
	submitForm = (e) => {
		e.preventDefault();
	};

	render() {
		return (
			<div style={{
				backgroundImage: `url(${searchBG})`,
				backgroundRepeat: 'no-repeat',
				backgroundPosition: 'center',
				backgroundSize: 'cover'
			}}>
				<Container>
					<Row>
						<Col>
							<Form inline style={{marginTop: 60, marginBottom: 60}} onSubmit={this.submitForm}>
								<InputGroup style={{width: '100%'}}>
									<InputGroupAddon className="input-group-prepend">
										<span className="input-group-text">
											<i className="fa fa-search"/>
										</span>
									</InputGroupAddon>
									<Input placeholder="Search..."/>
									<InputGroupAddon className="input-group-prepend">
										<span className="input-group-text">IN</span>
									</InputGroupAddon>
									<Input placeholder="HONG KONG"/>
									<InputGroupAddon className="input-group-prepend">
										<Button type="submit">
											SEARCH NOW
										</Button>
									</InputGroupAddon>
								</InputGroup>
							</Form>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(SearchForm);
