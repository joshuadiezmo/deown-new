import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
// import {test} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import SearchForm from './SearchForm';
import ItemsCarousel from './ItemsCarousel';
import Categories from './Categories';
import FeaturedList from './FeaturedList';
import HomeFooter from './HomeFooter';
import CategoriesMobile from './CategoriesMobile';

class Home extends Component {
	render() {
		return (
			<div>
				<SearchForm/>
				<Container>
					<Row>
						<Col>
							<ItemsCarousel/>
							<Categories/>
							<CategoriesMobile/>
							<FeaturedList/>
						</Col>
					</Row>
				</Container>
				<HomeFooter/>
			</div>
		);
	}
}

function mapStateToProps(state) {
	var {} = state;
	const props = {};
	return props;
}

function mapDispatchToProps(dispatch) {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
}

export default connect(mapStateToProps, mapDispatchToProps)(Home);
