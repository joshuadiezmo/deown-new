import React, {Component} from 'react';
import OwlCarousel from 'react-owl-carousel';

class Carousel extends Component {
	renderCarouselItem = () => {
		let {items} = this.props;
		return items.map((item, index) => {
			return (<div className="item" key={index}><h4>{index + 1}</h4></div>);
		});
	};

	changeIndex = (index) => {
		if (this.owl) {
			this.owl.to(index,500);
		}
	};

	shouldComponentUpdate(nextProps, nextState) {
		return false;
	}

	render() {
		return (
			<OwlCarousel className="owl-theme" margin={10} items={1} dots={false}
						 onChanged={this.props.onChanged || (() => {
						 })} ref={(e) => this.owl = e}>
				{this.renderCarouselItem()}
			</OwlCarousel>
		);
	}
}

export default Carousel
