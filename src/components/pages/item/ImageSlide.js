import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import Carousel from './Carousel';

class ImageSlide extends Component {
	state = {
		items: Array.apply(null, Array(Math.floor((Math.random() * 4) + 1))),
		index: 0
	};

	renderImagePreview = () => {
		let {items, index} = this.state;
		return items.map((item, _index) => {
			return (
				<div className={`item-preview ${index == _index ? 'active' : ''}`} key={_index}
					 onClick={() => this.carousel.changeIndex(_index)}>
					{_index + 1}<br/>
				</div>);
		});
	};

	render() {
		return (
			<div className="image-slide">
				<Carousel items={this.state.items} onChanged={(event) => this.setState({index: event.item.index})}
						  ref={e => this.carousel = e}/>
				<div className="image-slide-preview">
					{this.renderImagePreview()}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ImageSlide);
