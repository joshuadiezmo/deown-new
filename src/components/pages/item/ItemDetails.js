import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Link} from 'react-router-dom';

class ItemDetails extends Component {
	render() {
		return (
			<div className="idcon">
				<div className="item-details">
					<h2 className="name">Item Name</h2>
					<p className="price">HKD 12 per day</p>
					<div className="review-stars">
						<i className="fa fa-star"/>
						<i className="fa fa-star"/>
						<i className="fa fa-star"/>
						<i className="fa fa-star"/>
						<i className="fa fa-star"/>
					</div>
					<span className="review-text">0 Review</span>
				</div>
				<div className="edit">
					<Link to="#" style={{fontWeight: 700, fontSize: '13pt'}}>
						Edit
					</Link>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemDetails);
