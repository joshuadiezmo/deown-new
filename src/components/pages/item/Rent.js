import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col, FormGroup, Label, Input, Button} from 'reactstrap';

class Rent extends Component {
	render() {
		return (
			<div className="item-rent">
				<Row>
					<Col xs={12} md={6}>
						<FormGroup>
							<Label for="date-of-reciept">Date of Reciept</Label>
							<Input type="date" name="date-of-reciept" id="date-of-reciept"
								   placeholder="Date of Reciept"/>
						</FormGroup>
					</Col>
					<Col xs={12} md={6}>
						<FormGroup>
							<Label for="date-of-return">Date of Return</Label>
							<Input type="date" name="date-of-return" id="date-of-return" placeholder="Date of Return"/>
						</FormGroup>
					</Col>
				</Row>
				<Row>
					<Col xs={12}>
						<p style={{fontWeight: 'bold', margin: 0}}>Preferred Meetup Place</p>
						<p style={{fontSize: '10pt'}}>
							<i className="fa fa-map-marker"/>
							HONG KONG
						</p>
					</Col>
					<Col xs={12}>
						<p style={{fontWeight: 'bold', margin: 0}}>
							Meetup Details
						</p>
						<p style={{fontSize: '10pt', textAlign: 'justify',textJustify: 'inter-word'}}>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aliquam amet autem
							consectetur consequatur dolores iste magnam nam, nulla optio, pariatur perferendis possimus
							quam qui recusandae sequi sit veritatis voluptas?
						</p>
					</Col>
					<Col xs={12}>
						<h1 className="amount">
							Rental Fee HKD 10
						</h1>
					</Col>
					<Col xs={12}>
						<Button size="lg" color="primary" block>
							Make an offer
						</Button>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Rent);
