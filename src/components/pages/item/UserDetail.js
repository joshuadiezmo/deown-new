import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Button} from 'reactstrap';

class UserDetail extends Component {
	render() {
		return (
			<div className="user-details">
				<img src="http://via.placeholder.com/100x100" alt="user" className="profile-image"/>
				<div className="user-info-and-actions">
					<div className="user-info-container">
						<p>
							<span className="user-name">You</span>
							<span className="user-username">@username</span>
						</p>
						<div className="ratings-container">
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
							<i className="fa fa-star"/>
						</div>
					</div>
					<div className="actions">
						<div style={{width: '50%', float: 'left', paddingRight: 5}}>
							<Button outline color="primary" block style={{whiteSpace: 'normal'}}>
								View Profile
							</Button>
						</div>
						<div style={{width: '50%', float: 'left', paddingLeft: 5}}>
							<Button color="primary" block style={{whiteSpace: 'normal'}}>
								Send Message
							</Button>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(UserDetail);
