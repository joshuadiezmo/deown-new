import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import ImageSlide from './ImageSlide';
import UserDetail from './UserDetail';
import Rent from './Rent';
import ItemDetails from './ItemDetails';

class Item extends Component {
	itemDetails = (className) => {
		return (
			<Row className={className}>
				<Col xs={12}>
					<ItemDetails/>
				</Col>
			</Row>
		);
	};

	render() {
		return (
			<div className="page-item">
				<div className="header-bg"/>
				<Container>
					<Row style={{marginTop: 60}}>
						<Col md={4} xs={12} className="left-container">
							<ImageSlide/>
							{this.itemDetails("d-block d-md-none")}
							<UserDetail/>
							<Rent/>
						</Col>
						<Col md={8} xs={12} className="right-container">
							{this.itemDetails("d-none d-md-block")}
							<Row className="item-body">
								<Col>
									qweqwe
								</Col>
							</Row>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Item);
