import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import ProductListItem from '../../item/productListItem';

class ItemList extends Component {
	renderItems = () => {
		let {cat} = this.getJsonFromUrl();
		let items = Array.apply(null, Array(cat ? Math.floor((Math.random() * 15) + 1) : 20));
		return items.map((item, index) => {
			return (
				<Col xs={6} key={index} className="list-item-md">
					<ProductListItem/>
				</Col>
			);
		});
	};

	getJsonFromUrl = () => {
		var query = this.props.location.search.substr(1);
		var result = {};
		if (query != '')
			query.split("&").forEach(function (part) {
				var item = part.split("=");
				result[item[0]] = decodeURIComponent(item[1]);
			});
		return result;
	};

	render() {
		return (
			<div className="item-list">
				<Container>
					<Row>
						<Col>
							<h4 className="header">
								Results
							</h4>
						</Col>
					</Row>
					<Row>
						{this.renderItems()}
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(ItemList);
