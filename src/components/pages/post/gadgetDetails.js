import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Link} from 'react-router-dom';
import {Row, Col, FormGroup, Label, Input, FormText, Button} from 'reactstrap';

class GadgetDetails extends Component {
	render() {
		let props = Object.assign({}, this.props);
		return (
			<div {...props}>
				<Row>
					<Col md={6} xs={12}>
						<FormGroup>
							<Label for="name">Name</Label>
							<Input type="text" name="name" id="name" placeholder="Add a name to your post..."/>
						</FormGroup>
						<FormGroup>
							<Label for="category">Category</Label>
							<Input type="select" name="category" id="category">
								<option value="camera">Camera</option>
								<option value="camera_accessories">Camera Accessories</option>
								<option value="drone">Drone</option>
								<option value="drone_accessories">Drone Accessories</option>
								<option value="game_console">Game Console</option>
								<option value="others">Others</option>
							</Input>
						</FormGroup>
						<FormGroup>
							<Label for="brand">Brand</Label>
							<Input type="text" name="brand" id="brand" placeholder="Start typing a brand name..."/>
						</FormGroup>
						<FormGroup>
							<Label for="model">Model (optional)</Label>
							<Input type="text" name="model" id="model" placeholder="Model"/>
						</FormGroup>
						<FormGroup>
							<Label for="rent_price" style={{margin: 0}}>Rent price</Label>
							<FormText color="muted">
								(suggested rental price is around 3-5% of item retail price)
							</FormText>
							<Input type="number" name="rent_price" id="rent_price" placeholder="Rent Price"/>
						</FormGroup>
					</Col>
					<Col md={6} xs={12}>
						<FormGroup>
							<Label for="description">Description (optional)</Label>
							<Input type="textarea" name="description" id="description"
								   placeholder="Tell us more about your gadget..."
								   style={{maxHeight: 200, minHeight: 100}}/>
						</FormGroup>
						<FormGroup>
							<Label for="meetup_place">Preferred Meetup Place</Label>
							<Input type="text" name="meetup_place" id="meetup_place" placeholder="Hong Kong"/>
						</FormGroup>
						<FormGroup>
							<Label for="meetup_place_details">Meetup Place Details (optional)</Label>
							<Input type="textarea" name="meetup_place_details" id="meetup_place_details"
								   placeholder="Tell us more about meeting details with your client..."
								   style={{maxHeight: 200, minHeight: 100}}/>
						</FormGroup>
						<FormGroup check>
							<Label check>
								<Input type="checkbox" name="publish_facebook"/>{' '}
								<i className="fa fa-facebook-square" style={{color: '#3b5998'}}/>{' '}Publish to
								Facebook wall
							</Label>
						</FormGroup>
						<Button block size="sm" color="primary" style={{
							marginTop: 10,
							display: 'flex',
							flexDirection: 'row',
							justifyContent: 'space-between',
							alignItems: 'center'
						}}>
							<span>Post</span>
							<i className="fa fa-chevron-right"/>
						</Button>
						<FormText>
							By clicking this button you indicate that you have read and agree to the
							<Link to="#">Terms of Service</Link> and <Link to="#">Privacy Policy</Link>
						</FormText>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(GadgetDetails);
