import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import Nav from './nav';
import UploadImage from './uploadImage';
import GadgetDetails from './gadgetDetails';

class Post extends Component {
	state = {
		activeTab: 2
	};

	render() {
		let {activeTab} = this.state;
		return (
			<div className="page-post">
				<Container fluid>
					<Row style={{backgroundColor: '#eee', borderBottom: '.1px solid #ddd'}}>
						<Col>
							<Container>
								<Row>
									<Col>
										<Nav activeTab={activeTab}
											 onTabChange={(index) => this.setState({activeTab: index})}/>
									</Col>
								</Row>
							</Container>
						</Col>
					</Row>
					<Row style={{marginTop: 40}}>
						<Col>
							<Container>
								<Row>
									<Col>
										<UploadImage style={{display: activeTab == 1 ? 'block' : 'none'}}
													 onContinue={() => this.setState({activeTab: 2})}/>
										<GadgetDetails style={{display: activeTab == 2 ? 'block' : 'none'}}/>
									</Col>
								</Row>
							</Container>
						</Col>
					</Row>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Post);
