import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';

class Nav extends Component {
	render() {
		let {activeTab, onTabChange} = this.props;
		return (
			<div className="post-nav">
				<div className={`post-nav-item ${activeTab == 1 ? 'active' : ''}`}
					 onClick={() => {
						 if (onTabChange)
							 onTabChange(1)
					 }}>
					<div className="post-nav-item-container">
						<div>
							<div className="page-indicator">
								<span>1</span>
							</div>
						</div>
						<div className="text-container">
							<p className="title">
								Upload Photos
							</p>
							<p className="sub-title">
								You can either send us images or snap a photo of the
								logo from your phone.</p>
						</div>
					</div>
				</div>
				<div className={`post-nav-item ${activeTab == 2 ? 'active' : ''}`}
					 onClick={() => {
						 if (onTabChange)
							 onTabChange(2)
					 }}>
					<div className="post-nav-item-container">
						<div>
							<div className="page-indicator">
								<span>2</span>
							</div>
						</div>
						<div className="text-container">
							<p className="title">
								Enter Gadget Details
							</p>
							<p className="sub-title">
								Give the gadget details by filling up this simple form.
							</p>
						</div>
					</div>
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Nav);
