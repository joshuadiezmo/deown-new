import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col, Button} from 'reactstrap';

class UploadImage extends Component {
	render() {
		let {onContinue} = this.props;
		let props = Object.assign({}, this.props);
		delete props.onContinue;
		return (
			<div {...props}>
				<Row className="upload-image" style={{justifyContent: 'center'}}>
					<Col xs={12} sm={7} md={5} className="image-preview-container">
						<div className="image-preview">
							<span className="feature-photo">
								Featured Photo
							</span>
						</div>
						<div className="image-preview"/>
						<div className="image-preview"/>
						<div className="image-preview"/>
					</Col>
					<Col xs={12} md={7}>
						<h4 style={{margin: 0, fontSize: '14pt', fontWeight: 'bold'}}>
							PHOTO UPLOAD
						</h4>
						<p style={{margin: 0}}>
							Please take note that you can send up to 4 photos
						</p>
						<Button block size="sm" color="primary" style={{
							marginTop: 20,
							display: 'flex',
							flexDirection: 'row',
							justifyContent: 'space-between',
							alignItems: 'center'
						}} onClick={() => {
							if (onContinue)
								onContinue();
						}}>
							<span>Continue</span>
							<i className="fa fa-chevron-right"/>
						</Button>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(UploadImage);
