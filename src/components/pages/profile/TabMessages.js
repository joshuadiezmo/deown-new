import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';
import MessagesOnline from '../../MessagesOnline'

class TabMessages extends Component {
	render() {
		return (
			<div className="tab-messages">
				<Row noGutters style={{height:400}}>
					<Col md={4} xs={12} style={{border:'.1px solid #eee',}}>
						<MessagesOnline/>
					</Col>
					<Col md={8} xs={12} style={{border:'.1px solid #eee'}}>
						<div style={{width: '100%', height: 100}}/>
					</Col>
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(TabMessages);
