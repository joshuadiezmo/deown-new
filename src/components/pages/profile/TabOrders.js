import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Table} from 'reactstrap';

class TabOrders extends Component {
	render() {
		return (
			<div className="tab-orders">
				<h3 className="title">
					Order Request
				</h3>
				<span className="hint">These are orders for your listed gadgets</span>
				<Table bordered responsive>
					<thead>
					<tr>
						<th>ACTIONS</th>
						<th>PRODUCT</th>
						<th>PICKUP DETAILS</th>
						<th>PRICE</th>
						<th>DISCOUNT PRICE</th>
						<th>BORROWER</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							TO BE PICKED UP
						</td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							CANON CAMERA
						</td>
						<td>
							<p>Pickup Location: Mong Kok Rd, Hong Kong</p>
							<p>Pickup Date: 12/07/2017| Return Date: 12/08/2017</p>
							<p>Total Days: 2</p>
						</td>
						<td>HKD 100</td>
						<td></td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							<p className="username">@Jsuek_1206testing</p>
						</td>
					</tr>
					</tbody>
				</Table>

				<h3 className="title">
					MY REQUESTS
				</h3>
				<span className="hint">
					These are orders from you to the other owners
				</span>
				<Table bordered responsive>
					<thead>
					<tr>
						<th>ACTIONS</th>
						<th>PRODUCT</th>
						<th>PICKUP DETAILS</th>
						<th>PRICE</th>
						<th>DISCOUNT PRICE</th>
						<th>BORROWER</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td>
							TO BE PICKED UP
						</td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							CANON CAMERA
						</td>
						<td>
							<p>Pickup Location: Mong Kok Rd, Hong Kong</p>
							<p>Pickup Date: 12/07/2017| Return Date: 12/08/2017</p>
							<p>Total Days: 2</p>
						</td>
						<td>HKD 100</td>
						<td></td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							<p className="username">@Jsuek_1206testing</p>
						</td>
					</tr>
					<tr>
						<td>
							TO BE PICKED UP
						</td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							CANON CAMERA
						</td>
						<td>
							<p>Pickup Location: Mong Kok Rd, Hong Kong</p>
							<p>Pickup Date: 12/07/2017| Return Date: 12/08/2017</p>
							<p>Total Days: 2</p>
						</td>
						<td>HKD 100</td>
						<td></td>
						<td>
							<img src="http://via.placeholder.com/100x100" alt="username"/>
							<p className="username">@Jsuek_1206testing</p>
						</td>
					</tr>
					</tbody>
				</Table>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(TabOrders);
