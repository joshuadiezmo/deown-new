import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Row, Col} from 'reactstrap';
import ProductListItem from '../../item/productListItem';

class TabProfile extends Component {
	renderItems = () => {
		let items = Array.apply(null, Array(5));
		return items.map((item, index) => {
			return (
				<Col xs={6} key={index} className="list-item-md">
					<ProductListItem/>
				</Col>
			);
		});
	};

	render() {
		return (
			<div className="tab-profile">
				<h3 className="title">
					Recent Listing
				</h3>
				<Row style={{marginTop:10}}>
					{this.renderItems()}
				</Row>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(TabProfile);
