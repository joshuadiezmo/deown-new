import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Container, Row, Col} from 'reactstrap';
import Info from './info';

const cartBanner = require('../../../images/cart-banner.png');

class Profile extends Component {
	render() {
		return (
			<div className="page-profile">
				<div className="cart-banner" style={{backgroundImage: `url(${cartBanner})`}}>
					<Container>
						<Row style={{alignItems: 'flex-end', justifyContent: 'center'}}>
							<Col md={2} xs={6}>
								<div className="profile-image">
									<img src="http://via.placeholder.com/300x300"/>
								</div>
							</Col>
							<Col md={10} xs={12} className="header-info">
								<h2 className="name">MAKOY MIRASOL</h2>
								<span className="username">@makoy</span>
								<span className="divider">|</span>
								<span className="country">HK</span>
								<div style={{display: 'flex', flexDirection: 'row'}}>
									<div style={{marginRight: 5}}>
										<i className="fa fa-star"/>
										<i className="fa fa-star"/>
										<i className="fa fa-star"/>
										<i className="fa fa-star"/>
										<i className="fa fa-star"/>
									</div>
									<span style={{color: '#fff'}}>
										0 Review
									</span>
								</div>
							</Col>
						</Row>
					</Container>
				</div>
				<Container>
					<Info location={this.props.location} match={this.props.match}/>
				</Container>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
