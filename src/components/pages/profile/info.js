import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Route, Switch} from 'react-router-dom';
import {Row, Col} from 'reactstrap';
import Nav from './nav';
import TabProfile from './TabProfile';
import TabOrders from './TabOrders';
import TabMessages from './TabMessages';

class Info extends Component {
	render() {
		let {match, location} = this.props;
		return (
			<Row className="info">
				<Col>
					<Row>
						<Col md={2} xs={12}/>
						<Col md={10} xs={12}>
							<Nav location={location} match={match}/>
						</Col>
					</Row>
					<Row style={{marginTop:20}}>
						<Col>
							<Switch>
								<Route exact path={`${match.url}`} component={TabProfile}/>
								<Route path={`${match.url}/profile`} component={TabProfile}/>
								<Route path={`${match.url}/orders`} component={TabOrders}/>
								<Route path={`${match.url}/messages`} component={TabMessages}/>
							</Switch>
						</Col>
					</Row>
				</Col>
			</Row>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Info);
