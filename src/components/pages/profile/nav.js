import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
//import {} from 'actions';
import {Nav as NavLib, NavItem, NavLink} from 'reactstrap';
import {LinkContainer} from 'react-router-bootstrap';

class Nav extends Component {
	renderLink = () => {
		let items = ['profile', 'orders', 'messages', 'account_settings', 'billing_settings'];
		return items.map(item => {
			let {pathname} = this.props.location;
			let baseUrl = '/user/makoy';
			return (
				<NavItem key={item}
						 className={pathname == baseUrl + '/' + item || ((pathname == baseUrl || pathname == baseUrl + '/') && item == 'profile') ? 'active' : ''}>
					<LinkContainer to={{
						pathname: '/user/makoy/' + item,
					}} activeClassName="">
						<NavLink>
							{item.prettify()}
						</NavLink>
					</LinkContainer>
				</NavItem>
			)
		});
	};

	render() {
		return (
			<NavLib tabs fill>
				{this.renderLink()}
			</NavLib>
		);
	}
}

const mapStateToProps = (state) => {
	//const {} = state;
	const props = {};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(Nav);
