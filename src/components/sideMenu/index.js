import React, {Component} from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {schemas} from 'actions';
import {Link} from 'react-router-dom';

class SideMenu extends Component {
	componentWillMount() {
		this.props.actions.schemas();
	}

	render() {
		let schemas = this.props.schemas;
		return (
			<div style={{width: 250}} className="side-menu dark">
				<ul>
					{
						schemas.data.map((schema) => {
							return (
								<li key={`side-menu-${schema.className}`}>
									<Link to={'/class/' + schema.className}>
										{schema.className.prettify()}
									</Link>
								</li>
							)
						})
					}
				</ul>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	const {schemas} = state;
	const props = {schemas};
	return props;
};

const mapDispatchToProps = (dispatch) => {
	const actions = {schemas};
	const actionMap = {
		actions: bindActionCreators(actions, dispatch)
	};
	return actionMap;
};

export default connect(mapStateToProps, mapDispatchToProps)(SideMenu);
