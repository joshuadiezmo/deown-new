String.prototype.prettify = function () {
	return this.replace(/([A-Z])/g, ' $1').replace(/_/g, ' ').replace(/^./, function (str) {
		return str.toUpperCase();
	});
};
