let PARSE_SERVER_URL = process.env.PARSE_SERVER_URL;
let PARSE_APP_ID = process.env.PARSE_APP_ID;
let PARSE_MASTER_KEY = process.env.PARSE_MASTER_KEY;
const axios = require('axios');

var instance = axios.create({
	baseURL: PARSE_SERVER_URL,
	headers: {'X-Parse-Application-Id': PARSE_APP_ID, 'X-Parse-Master-Key': PARSE_MASTER_KEY}
});

module.exports = instance;
