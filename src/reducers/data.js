import {DATA_FETCHING, DATA_FETCHED, DATA_ERROR} from '../actions/const';

const initialState = {data: {}, loading: false, error: null};

function reducer(state = initialState, action) {
	/* Keep the reducer clean - do not mutate the original state. */
	const nextState = Object.assign({}, state);
	let {className, page} = action;
	switch (action.type) {

		case DATA_FETCHING: {
			return {...nextState, loading: true};
		}
		case DATA_FETCHED: {
			let data = nextState.data;
			data[className] = data[className] || {};
			data[className][page] = data[className][page] || [];
			data[className][page] = action.data;
			return {...nextState, data, error: null, loading: false};
		}
		case DATA_ERROR: {
			return {...nextState, loading: false, error: action.error};
		}

		default: {
			/* Return original state if no actions were consumed. */
			return state;
		}
	}
}

module.exports = reducer;
