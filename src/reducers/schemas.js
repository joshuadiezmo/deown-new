/* Define your initial state here.
 *
 * If you change the type from object to something else, do not forget to update
 * src/container/App.js accordingly.
 */
import {SCHEMA_FETCHED, SCHEMA_FETCHING, SCHEMA_ERROR} from '../actions/const';

const initialState = {data: [], isLoading: false, error: null};

function reducer(state = initialState, action) {
	/* Keep the reducer clean - do not mutate the original state. */
	const nextState = Object.assign({}, state);

	switch (action.type) {

		case SCHEMA_FETCHING: {
			return {...nextState, isLoading: true};
		}

		case SCHEMA_FETCHED: {
			return {...nextState, config: action.config, data: action.data, isLoading: false};
		}

		case SCHEMA_ERROR: {
			return {...nextState, error: action.error, isLoading: false};
		}

		default: {
			return nextState;
		}
	}
}

module.exports = reducer;
